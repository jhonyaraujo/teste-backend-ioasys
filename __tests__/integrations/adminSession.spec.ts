export {}

const app = require('../../src/app').express
const { User } = require('../../src/app/models')
import request from 'supertest'

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: '.env.development',
  })
} else {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
  })
}

describe('AdminSession', () => {
  it('should answer 200 in the post request for this route', async () => {
    const response = await request(app)
      .post('/admin/users')
      .send({
        name: 'jhony araujo',
        email: 'alexialima2488@gmail.com',
        password: 'jhony2488',
      })
      .set({ Authorization: `Bearer ${process.env.APP_SECRET}` })

    expect(response.status).toBe(200)
  }, 9999)
  it('should answer 401 in the post request for this route', async () => {
    const response = await request(app)
      .post('/admin/users')
      .send({
        name: 'jhony araujo',
        email: 'alexialima2488@gmail.com',
        password: 'jhony2488',
      })
      .set({ Authorization: `Bearer 687676787867867` })

    expect(response.status).toBe(401)
  })

  it('should answer 200 in the post request for this route', async () => {
    /* const user = await User.findOrCreate({
      where: {
        name: 'jhony araujo',
        email: 'jhonyalexialima2488@gmail.com',
        password_hash: 'jhony2488',
        admin: true,
      },
    })*/

    const response = await request(app)
      .post('/admin/films')
      .send({
        name: 'Jhony Jhony ',
        directorName: 'Josivaldo Teodoro',
        genre: ['suspense', 'Drama'],
        actores: [{ name: 'Milca Ester' }, { name: 'Alexia Lima' }],
        description:
          'desesesereesestrsdrssrsrrsrsersersersersersersersersersersesesersreserserserserserser',
      })
      .set({
        Authorization: `Bearer ${process.env.APP_SECRET}`,
        user_id: 2,
      })

    expect(response.status).toBe(200)
  })
  it('should answer 401 in the post request for this route', async () => {
    const response = await request(app)
      .post('/admin/films')
      .send({
        name: 'Jhony Jhony ',
        directorName: 'Josivaldo Teodoro',
        genre: ['suspense', 'Drama'],
        actores: [{ name: 'Milca Ester' }, { name: 'Alexia Lima' }],
        description:
          'desesesereesestrsdrssrsrrsrsersersersersersersersersersersesesersreserserserserserser',
      })
      .set({ Authorization: `Bearer 687676787867867` })

    expect(response.status).toBe(401)
  })
})
