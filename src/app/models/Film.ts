module.exports = (sequelize, DataTypes) => {
  const Film = sequelize.define(
    'Film',
    {
      director_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      slug: DataTypes.STRING,
      thumbnail: DataTypes.STRING,
      description: DataTypes.TEXT,
      average_votes: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'films',
      timestamps: true,
    }
  )
  Film.associate = function (models) {
    Film.hasMany(models.Voto, {
      foreignKey: 'film_id',
      as: 'film_votos',
    })
    Film.belongsTo(models.Director, {
      foreignKey: 'director_id',
      as: 'film_director',
    })
    Film.belongsToMany(models.Actor, {
      through: 'actors_films',
      foreignKey: 'film_id',
      as: 'film_actors',
    })
    Film.belongsToMany(models.Genre, {
      through: 'genres_films',
      foreignKey: 'film_id',
      as: 'film_genres',
    })
  }
  return Film
}
