module.exports = (sequelize, DataTypes) => {
  const Genre = sequelize.define(
    'Genre',
    {
      name: DataTypes.STRING,
      slug: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'genres',
      timestamps: true,
    }
  )
  Genre.associate = function (models) {
    Genre.belongsToMany(models.Film, {
      through: 'genres_films',
      foreignKey: 'genre_id',
      as: 'genre_films',
    })
  }
  return Genre
}
