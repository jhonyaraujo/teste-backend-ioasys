module.exports = (sequelize, DataTypes) => {
  const Actor = sequelize.define(
    'Actor',
    {
      name: DataTypes.STRING,
      photo: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'actors',
      timestamps: true,
    }
  )
  Actor.associate = function (models) {
    Actor.belongsToMany(models.Film, {
      through: 'actors_films',
      foreignKey: 'actor_id',
      as: 'actor-films',
    })
  }
  return Actor
}
