export {}

const jwt = require('jsonwebtoken')
const User = require('../models/User')

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: '.env.development',
  })
} else {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
  })
}

module.exports = async (req, res, next) => {
  const authHeader = req.headers.authorization

  if (!authHeader) {
    return res.status(401).json({ message: 'Token not provided' })
  }
  const [, token] = authHeader.split(' ')

  const jwtApp = jwt.sign(
    {
      token,
    },
    token
  )
  try {
    const verified = jwt.verify(jwtApp, process.env.APP_SECRET)
    req.user = verified
    if (req.user) {
      next()
    }
  } catch (error) {
    return res.status(401).json({ message: 'Token Invalid', error })
  }
}
