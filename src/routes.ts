import express from 'express'
const multer = require('multer')
const multerConfig = require('./config/multer')
const authMiddleware = require('./app/middlewares/auth')
const UsersControllers = require('./app/controllers/UsersControllers')
const FilmsControllers = require('./app/controllers/FilmsControllers')
const AdminControllers = require('./app/controllers/AdminControllers')

const router = express.Router()

router.use(authMiddleware)
//default
router.get('/', (req, res) => {
  res.json({ version: '1.0.0' })
})

//users
router.get('/users/:id_users',  UsersControllers.index)

router.post('/users/login', UsersControllers.login)
router.post(
  '/users',
  multer(multerConfig).single('file'),
  UsersControllers.create
)

router.put(
  '/users/:id_users',
  multer(multerConfig).single('file'),
  UsersControllers.update
)

router.delete('/users/:id_users', UsersControllers.delete)

//films
router.get('/films', FilmsControllers.index)

router.post(
  '/films/:film_id/users/:user_id',
  FilmsControllers.createVotos
)
//Admin
router.post(
  '/admin/users',
  multer(multerConfig).single('file'),
  AdminControllers.createAdminUser
)

router.post(
  '/admin/films',
  AdminControllers.createFilms
)

module.exports = router
