module.exports = (sequelize, DataTypes) => {
  const Director = sequelize.define(
    'Director',
    {
      name: DataTypes.STRING,
      photo: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'directors',
      timestamps: true,
    }
  )
  Director.associate = function (models) {
    Director.hasMany(models.Film, {
      foreignKey: 'director_id',
      as: 'director_films',
    })
  }

  return Director
}
