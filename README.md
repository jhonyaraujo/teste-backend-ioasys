
# API challange ioasys films


## Tabela de conteúdos
   
- [Sobre](#---api)

- [Tabela de Conteudo](#tabela-de-conteudo)

- [Features](#----features)

- [Demonstração da aplicação](#----demonstração-da-aplicação)

- [Como usar](#como-usar)

  - [Pre Requisitos](#pré-requisitos)

  - [Instalação](#instalação)

  - [Rodando a API](#-rodando-a-api)

- [Testes](#-testes)

- [Tecnologias](#-tecnologias)

 
## Features

- [x] Admin
- [x] Cadastro
- [x] Edição
- [x] Exclusão lógica (Desativação)

- [x] Usuário
- [x] Cadastro
- [x] Edição
- [x] Exclusão lógica (Desativação)

- [x] Filmes
- [x] Cadastro (Somente um usuário administrador poderá realizar esse cadastro)
- [x] Voto (A contagem dos votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
- [x] Listagem (deverá ter filtro por diretor, nome, gênero e/ou atores)
- [x] Detalhe do filme trazendo todas as informações sobre o filme, inclusive a média dos votos

 
## Demonstração da aplicação

[Link Da Aplicação](https://nodejs.org/en/)

## Como usar

### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Node.js](https://nodejs.org/en/),[MySQL](https://www.mysql.com/).
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)

### Instalação

```bash
# Clone este repositório
$ git clone <https://jhonyaraujo@bitbucket.org/jhonyaraujo/teste-backend-ioasys.git>

# Acesse a pasta do projeto no terminal/cmd
$ cd teste-backend-ioasys

# Instale as dependências
$ npm install || yarn add

```

### 🎲 Rodando a API

```bash
# Execute a aplicação em modo de desenvolvimento sem utilizar o docker
$ npm run dev || yarn dev

# O servidor inciará na porta:3000 - acesse <http://localhost:8080>
```

## 🛠 Testes

```bash
# Executar os testes
$ npm run test || yarn test

# Executar testes de estresse da API
$ npm run test-stress || yarn test-stress

```

## 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Node.js](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)
- [Express.js](https://expressjs.com/pt-br/)
- [Sequelize](https://sequelize.org/)
- [Sequelize-cli](https://www.npmjs.com/package/sequelize-cli)
- [Jest](https://jestjs.io/)
- [JWT](https://jwt.io/)
- [Eslint](https://eslint.org/)
- [Prettier](https://prettier.io/)


