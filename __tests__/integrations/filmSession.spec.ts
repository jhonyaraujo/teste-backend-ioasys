export {}

const app = require('../../src/app').express
import request from 'supertest'

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: '.env.development',
  })
} else {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
  })
}

describe('FilmSession', () => {
  it('should answer 200 in the get request for this route', async () => {
    const response = await request(app)
      .get('/films')
      .set({ Authorization: `Bearer ${process.env.APP_SECRET}` })

    expect(response.status).toBe(200)
  })
  it('should answer 401 in the get request for this route', async () => {
    const response = await request(app)
      .get('/films')
      .set({ Authorization: `Bearer 687676787867867` })

    expect(response.status).toBe(401)
  })
  it('should answer 200 in the post request for this route', async () => {
    const response = await request(app)
      .post(`/films/${1}/users/${1}`)
      .send({
        amount: 3,
      })
      .set({ Authorization: `Bearer ${process.env.APP_SECRET}` })

    expect(response.status).toBe(200)
  })
  it('should answer 401 in the post request for this route', async () => {
    const response = await request(app)
      .post(`/films/${1}/users/${1}`)
      .send({
        amount: 3,
      })
      .set({ Authorization: `Bearer 687676787867867` })

    expect(response.status).toBe(401)
  })
})
