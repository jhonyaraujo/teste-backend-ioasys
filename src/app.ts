export {}

import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: '.env.development',
  })
} else {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
  })
}

class AppController {
  public express
  public port: string
  public host: string
  constructor() {
    this.express = express()

    this.port = process.env.APP_PORT_SERVER
    this.host = process.env.APP_HOST

    this.bodyParser()
    this.middleware()
    this.routes()
  }
  middleware() {
    this.express.use(express.json())
  }
  routes() {
    this.express.use(require('./routes'))
  }
  bodyParser() {
    this.express.use(bodyParser.urlencoded({ extended: false }))
    this.express.use(bodyParser.json())
  }
  morganUse() {
    this.express.use(morgan('dev'))
  }
  server() {
    this.express.listen(this.port, () => {
      if (process.env.APP_PORT_SERVER == '8080') {
        console.log('servidor rodando em desenvolvimento')
      } else {
        console.log('servidor rodando em produção')
      }
    })
  }
}

module.exports = new AppController()
