export {}

const app = require('../../src/app').express
import request from 'supertest'

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: '.env.development',
  })
} else {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
  })
}

describe('UserSession', () => {
  it('should answer 200 in the post user creation request for this route', async () => {
    const response = await request(app)
      .post(`/users`)
      .send({
        name: 'jhony',
        email: 'jhon.araujo2488@gmail.com',
        password: '12345678',
      })
      .set({ Authorization: `Bearer ${process.env.APP_SECRET}` })

    expect(response.status).toBe(200)
  }, 9999)

  it('should answer 401 in the post user creation request for this route', async () => {
    const response = await request(app)
      .post(`/users`)
      .send({
        name: 'jhony',
        email: 'jhon.araujo2488@gmail.com',
        password: '12345678',
      })
      .set({ Authorization: `Bearer t7575675765` })

    expect(response.status).toBe(401)
  })

  it('should answer 200 in the put user update request for this route', async () => {
    const response = await request(app)
      .put(`/users/${1}`)
      .send({
        name: 'jhony',
        email: 'jhon.araujo2488@gmail.com',
        password: '12345678',
      })
      .set({ Authorization: `Bearer ${process.env.APP_SECRET}` })

    expect(response.status).toBe(200)
  })

  it('should answer 401 in the put user update request for this route', async () => {
    const response = await request(app)
      .put(`/users/${1}`)
      .send({
        name: 'jhony',
        email: 'jhon.araujo2488@gmail.com',
        password: '12345678',
      })
      .set({ Authorization: `Bearer 7678576567567` })

    expect(response.status).toBe(401)
  })

  it('should answer 200 in the get request for this route', async () => {
    const response = await request(app)
      .get(`/users/${1}`)
      .set({ Authorization: `Bearer ${process.env.APP_SECRET}` })

    expect(response.status).toBe(200)
  })

  it('should answer 401 in the get request for this route', async () => {
    const response = await request(app)
      .get(`/users/${1}`)
      .set({ Authorization: `Bearer 786678676776` })

    expect(response.status).toBe(401)
  })

  it('should answer 200 in the delete user update request for this route', async () => {
    const response = await request(app)
      .delete(`/users/${1}`)
      .set({ Authorization: `Bearer ${process.env.APP_SECRET}` })

    expect(response.status).toBe(200)
  })

  it('should answer 401 in the delete user update request for this route', async () => {
    const response = await request(app)
      .delete(`/users/${1}`)
      .set({ Authorization: `Bearer 76757567567` })

    expect(response.status).toBe(401)
  })
})
