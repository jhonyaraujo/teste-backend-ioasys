export {}

const { User, Film, Director, Actor, Genre } = require('../models')
const bcryptjs = require('bcryptjs')
const slugify = require('slugify')

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: '.env.development',
  })
} else {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
  })
}

interface CRUDAdmin {
  createAdminUser(req, res): void
  createFilms(req, res): void
}

class AdminsControllers implements CRUDAdmin {
  async createAdminUser(req, res) {
    const { name, email, password, file } = req.body

    const salt = await bcryptjs.genSaltSync(10)
    let password_hash = await bcryptjs.hashSync(password, salt)

    User.findAll({ where: { email } })
      .then((user) => {
        if (user == undefined || user == null || user == NaN) {
          if (file) {
            User.create({
              name,
              email,
              password_hash,
              photo: file.path,
              admin: true,
            })
              .then((userCreate) => {
                res.header().json({
                  error: null,
                  data: { id: userCreate.id },
                })
              })
              .catch((error) => {
                res.json(error)
              })
          } else {
            User.create({
              name,
              email,
              password_hash,
              admin: true,
            })
              .then((userCreate) => {
                res.header().json({
                  error: null,
                  data: { id: userCreate.id },
                })
              })
              .catch((error) => {
                res.json(error)
              })
          }
        }
      })
      .catch((error) => {
        res.json({ error, response: 'Esse Email já está sendo utilizado' })
      })
  }
  async createFilms(req, res) {
    const { name, directorName, genre, actores, description } = req.body
    let filmsGet
    const genreGet = genre
    const ActorGet = actores
    const idUser = req.header('user_id')
    if (idUser) {
      User.findOne({
        where: {
          id: idUser,
        },
      })
        .then((user) => {
          if (user.admin != true) {
            return res.status(401).json({ message: 'User not Admin' })
          }
        })
        .catch((error) => {
          return res.status(401).json(error)
        })
    } else {
      return res.status(401).json({ message: 'You not Admin' })
    }

    const DirectorGetOrCreate = await Director.findOrCreate({
      where: {
        name: directorName.toLowerCase(),
      },
    })
      .then(async (director) => {
        filmsGet = await Film.create({
          director_id: director.id,
          name: name.toLowerCase(),
          description,
          average_votes: 0.0,
          slug: slugify(name.toLowerCase()),
        })
        let genreUnic
        for (genreUnic in genreGet) {
          const [genre] = await Genre.findOrCreate({
            where: {
              name: genreUnic.toLowerCase(),
              slug: slugify(genreUnic.toLowerCase()),
            },
          })
          await filmsGet.addGenre(genre)
        }

        let actor
        for (actor in ActorGet) {
          if (actor.photo != undefined && actor.photo != null) {
            const [actores] = await Actor.findOrCreate({
              where: {
                name: actor.name.toLowerCase(),
                photo: actor.photo,
              },
            })

            await filmsGet.addActor(actores)
          } else {
            const [actores] = await Actor.findOrCreate({
              where: {
                name: actor.name.toLowerCase(),
              },
            })

            await filmsGet.addActor(actores)
          }
        }
        return res.json({ films:filmsGet, actores, genre, DirectorGetOrCreate })
      })
      .catch((error) => {
        res.json(error)
      })
  }
}

module.exports = new AdminsControllers()
