module.exports = (sequelize, DataTypes) => {
  const Voto = sequelize.define(
    'Voto',
    {
      amount: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
      tableName: 'voting',
      timestamps: true,
    }
  )
  Voto.associate = function (models) {
    Voto.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user_votos',
    })
    Voto.belongsTo(models.Film, {
      foreignKey: 'film_id',
      as: 'film_votos',
    })
  }
  return Voto
}
