module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      admin: DataTypes.BOOLEAN,
      active: DataTypes.BOOLEAN,
      name: DataTypes.STRING,
      photo: DataTypes.STRING,
      email: DataTypes.STRING,
      password_hash: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'users',
      timestamps: true,
    }
  )
  User.associate = function (models) {
      User.hasMany(models.Voto, {
      foreignKey: 'user_id',
      as: 'user_votos',
    })
  }

  return User
}
