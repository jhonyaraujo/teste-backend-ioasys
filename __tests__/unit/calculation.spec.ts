export {}

const calculations = require('../../src/app/utils/calculations')

describe('Calculation', () => {
  it('should return the average of the vote values ​​in a vote object array', async () => {
    expect(
      calculations.calculationMediaVotos([
        {
          amount: 4,
        },
        {
          amount: 4,
        },
        {
          amount: 2,
        },
        {
          amount: 3,
        },
      ])
    ).toBe(3.25)
  })
})
