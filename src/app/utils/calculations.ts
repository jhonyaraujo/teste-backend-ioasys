export {}

require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
})

interface CalculationInterface {
  calculationMediaVotos(votos: number[]): void
}

class Calculation implements CalculationInterface {
  calculationMediaVotos(votos) {
    let filterNumberVotos = votos.map((voto) => {
      return voto.amount
    })

    let total = filterNumberVotos.reduce((total, numero) => {
      return total + numero
    }, 0)
    total = total / filterNumberVotos.length
    return total
  }
}

module.exports = new Calculation()
