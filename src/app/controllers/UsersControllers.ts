export {}

const { User } = require('../models')
const bcryptjs = require('bcryptjs')

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: '.env.development',
  })
} else {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
  })
}

interface CRUDUser {
  index(req, res): void
  login(req, res): void
  create(req, res): void
  update(req, res): void
  delete(req, res): void
}

class UsersControllers implements CRUDUser {
  async index(req, res) {
    const { id_users } = req.params

    User.findOne({
      where: {
        id: id_users,
      },
    })
      .then((user) => {
        res.json(user)
      })
      .catch((error) => {
        res.json(error)
      })
  }

  async login(req, res) {
    const { email, password } = req.body

    User.findOne({
      where: {
        email,
      },
    })
      .then(async (user) => {
        if (user.active == false) {
          return res.json({ response: 'User excluded' })
        }
        if (user != undefined || user != null || user != NaN) {
          let correct = bcryptjs.compareSync(password, user.password_hash)

          if (correct) {
            if (user.admin) {
              return res.header().json({
                error: null,
                data: { id: user.id, admin: user.admin },
              })
            }
            res.header().json({
              error: null,
              data: { id: user.id },
            })
          } else {
            res.json({ response: 'Incorrect Password' })
          }
        } else {
          res.json({ response: 'User Not Found' })
        }
      })
      .catch((error) => {
        res.json(error)
      })
  }

  async create(req, res) {
    const { name, email, password, file } = req.body

    const salt = await bcryptjs.genSaltSync(10)
    let password_hash = await bcryptjs.hashSync(password, salt)

    User.findAll({ where: { email } })
      .then((user) => {
        if (user == undefined || user == null || user == NaN) {
          if (file) {
            User.create({
              name,
              email,
              password_hash,
              photo: file.path,
            })
              .then((userCreate) => {
                res.header().json({
                  error: null,
                  data: { id: userCreate.id },
                })
              })
              .catch((error) => {
                res.json(error)
              })
          } else {
            User.create({
              name,
              email,
              password_hash,
            })
              .then((userCreate) => {
                res.header().json({
                  error: null,
                  data: { id: userCreate.id },
                })
              })
              .catch((error) => {
                res.json(error)
              })
          }
        }
      })
      .catch((error) => {
        res.json({ error, response: 'Esse Email já está sendo utilizado' })
      })
  }

  async update(req, res) {
    const { name, email, password, photo } = req.body
    const { id_users } = req.params

    const salt = await bcryptjs.genSaltSync(10)
    let password_hash = await bcryptjs.hashSync(password, salt)

    User.findOne({ where: { email } })
      .then((user) => {
        if (photo == null || photo == undefined || photo == NaN) {
          User.update(
            {
              name,
              email,
              password_hash,
            },
            { where: { id: id_users } }
          )
            .then((updateUser) => {
              res.header().json({
                error: null,
                data: { id: updateUser.id },
              })
            })
            .catch((error) => {
              res.json(error)
            })
        } else {
          User.update(
            {
              name,
              email,
              password_hash,
              photo,
            },
            { where: { id: id_users } }
          )
            .then((updateUser) => {
              res.header().json({
                error: null,
                data: { id: updateUser.id },
              })
            })
            .catch((error) => {
              res.json(error)
            })
        }
      })
      .catch((error) => {
        res.json({ error, response: 'Esse Email já está sendo utilizado' })
      })
  }

  async delete(req, res) {
    const { id_users } = req.params

    if (id_users != null && id_users != undefined && id_users != NaN) {
      User.update(
        {
          active: false,
        },
        { where: { id: id_users } }
      )
        .then((deleteUser) => {
          res.json(deleteUser)
        })
        .catch((error) => {
          res.json(error)
        })
    } else {
      res.json({ response: 'Invalid Id ' })
    }
  }
}

module.exports = new UsersControllers()
