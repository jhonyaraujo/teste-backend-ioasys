export {}

const { Film, Voto, Actor, Genre, Director } = require('../models')
const Calculation = require('../utils/calculations')

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({
    path: '.env.development',
  })
} else {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env',
  })
}

interface CRUDFilms {
  index(req, res): void
  createVotos(req, res): void
}

class FilmsControllers implements CRUDFilms {
  async index(req, res) {
    const { directorName, name, genre, actorName, page } = req.query
    const pageGet = page
    let offset = 0
    let limit = 10

    let where
    where = {}
    if (directorName) where.film_director = directorName.toLowerCase()
    if (genre) where.film_genres = genre.toLowerCase()
    if (name) where.name = name.toLowerCase()
    if (actorName) where.film_actors = actorName.toLowerCase()
    else if (!directorName && !genre && !name && !actorName) {
      where = undefined
    }

    Film.findAndCountAll({
      where,
      order: [
        ['id', 'DESC'],
        ['name', 'ASC'],
      ],
      limit,
      offset,
      include: [
        {
          model: Director,
          as: 'film_director',
        },
        {
          model: Actor,
          as: 'film_actors',
        },
        {
          model: Genre,
          as: 'film_genres',
        },
        {
          model: Voto,
          association: 'film_votos',
        },
      ],
    })
      .then((films) => {
        let next
        if (offset + limit >= films.count) {
          next = false
        } else {
          next = true
        }
        let filmsUpdate = films.rows.map(async (film) => {
          let unicFilm = await Film.update(
            {
              average_votes: Calculation.calculationMediaVotos(film.film_votos),
            },
            { where: { name: film.name, genre: film.genre } }
          )

          return unicFilm
        })

        let result = {
          page: pageGet,
          next,
          films: filmsUpdate,
        }
        res.json({
          next: result.next,
          page: parseInt(result.page),
          films: result.films,
        })
      })
      .catch((error) => {
        res.json(error)
      })
  }
  async createVotos(req, res) {
    const { user_id, film_id } = req.params
    const { amount } = req.body

    Voto.create({
      user_id,
      film_id,
      amount,
    })
      .then((voto) => {
        res.json(voto)
      })
      .catch((error) => {
        res.json(error)
      })
  }
}

module.exports = new FilmsControllers()
